This project is a version of [Nick Jenkins's original code](https://github.com/Qwerfjkl/Missing-Redirects-Project). I rewrote the PHP code in python, to run on Toolforge.

Database structure
==================

| Tables                  | Description                                                                                |  
| ----------------------- | ------------------------------------------------------------------------------------------ |
| badlinks                | Stores titles that resolve to existing Wikipedia titles, but don't directly match          | 
| badtitles               | Stores titles that users have flagged as being unsuitable for a redirect                   |
| key_value_pairs         | An indexed list of the mainspace titles, and whether they are articles, redirects, or dabs |
| link_map                | A map of links and their targets, scraped from all off the articles                        |
| potential_dabs          | Currently unused. Generated from link_map.                                                 |
| potential_redirects     | A list of redirect suggestions generated from link_map.                                    |

File contents
=============

| File                   | Description |
| ---------------------- | ----------- |
| jobs.yaml              | Controls the cron jobs. |
| latest_dump.txt        | Lists the latest dump to work on. Updated by update_latest_dump.py, which is run once a month in jobs.yaml |
| pwb_venv.sh            | Setup script to create the venv for python to run in. This contains all the requirements. |
| lib.py                 | Contains basic shared functions between scripts. |
| reset_database.py      | Empties all of the databases in preparation for a new run. |
| create_memory_index.py | Populates key_value_pairs based on an XML dump. Takes around a day to run on enwiki. |
| index_dabs.py | Updates key_value_pairs with disambiguation pages queried from the API, because it's much more reliable than looking at the wikitext. |
| create_link_map.py     | Creates a map (link_map) of all the links on mainspace articles that fit the criteria (around 80 million on enwiki). Takes four to five days to run. |
| save_suggestions.py    | Generates potential_redirects and potential_dabs based on the link_map, using badlinks and badtitles as blacklists. potential_redirects is queried by the webserver to fetch suggestions. |
| app.py                 | This is the script that the server runs, using Flask. |
| base.html              | The base webpage styling and code. |
| index.html             | The main page HTML. |
| create_redirects.html  | The main client-side code file, which handles the vast majority of the client. |

Running on Toolforge
====================
I assume you already know how to setup a tool on Toolforge. If not, there are instructions at https://wikitech.wikimedia.org/wiki/Help:Toolforge/Quickstart.

First, clone this repository. My coded uses 's55593__PAGES' as the database name, from my database username and PAGES. The database username and password can be found in replica.my.cnf (unique for each tool). To create the database, become your tool account, type sql tools to open the MariaDB console. Then run CREATE DATABASE CREDENTIALUSER__DBNAME;, replacing CREDENTIALUSER with your databse username and DBNAME with whatever you want to name your database.
You will then need to update the code to use this new database name.

Next run the setup script pwb_venv.sh, which contains all of the requirements for the scripts. There are instructions on this at https://wikitech.wikimedia.org/wiki/Help:Toolforge/Running_Pywikibot_scripts_(advanced). The code doesn't actually depend on pywikibot for anything, so configuring it is unnecessary.

Next, to setup the webserver, follow the instructions at https://wikitech.wikimedia.org/wiki/Help:Toolforge/My_first_Flask_OAuth_tool. All requirements have already been includedin www/python/src/requirements.txt

Starting the webservice right now won't do much, because the database hasn't been generated yet.

To fix that, become the tool and run "toolforge jobs load jobs.yaml". This will set the database generator to run oncce a month. You can modify the scheduling in jobs.yaml; https://crontab.guru/ may be helpful.
These programs take a week to run on enwiki.

I have not tested running the database fully, so there are likely bugs.

Running on another server
=========================

The code is designed to be run on Toolforge, but it can be run anywhere. The main change to the code will be using "pymysql" for handling database queries, rather than the "toolforge" module the code uses.



