#!/bin/bash

# clean-up directories if they already exist
rm -fdr pwbvenv $HOME/pywikibot-core

# create a virtual environment
python3 -m venv pwbvenv

# activate it
source pwbvenv/bin/activate

# clone Pywikibot
git clone --recursive --branch stable "https://gerrit.wikimedia.org/r/pywikibot/core" $HOME/pywikibot-core

# install dependencies
pip install --upgrade pip setuptools wheel
pip install $HOME/pywikibot-core[mwoauth,mysql]  # update as needed
pip install requests
pip install lxml
pip install toolforge
pip install -U memory_profiler # any other dependencies here