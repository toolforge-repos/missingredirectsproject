import re
import toolforge

# constants
cutoff = 5 # global cutoff
REDIRECT_PERCENTAGE = 90 # when 90% of targets are the same, treat as a dab instead of a redirect
DISAMBIG_CUTOFF = 5
BATCH_SIZE = 10 # batch to save at a time

initial_query = f"""
    SELECT
        label,
        COUNT(*) AS count
    FROM
        redirect_candidate
    GROUP BY
        label
    HAVING
        count > {cutoff}
    ORDER BY
        count DESC,
        target,
        label
"""



connection = toolforge.toolsdb('s55593__PAGES')
with connection.cursor() as cursor:
    cursor.execute(initial_query)
    initial_results = cursor.fetchall()

# titles that would not be suitable as redirects
with connection.cursor() as cursor:
    cursor.execute("SELECT * FROM badtitles")
    badtitles = cursor.fetchall()

# titles that the client has previously detected resolve to existing Wikipedia titles, but don't exactly match
with connection.cursor() as cursor:
    cursor.execute("SELECT * FROM badlinks")
    badlinks = cursor.fetchall()

processed_count = 0
# Iterate over labels
for row in initial_results:
    processed_count += 1
    label, total_count  = row
    if label in badtitles or label in badlinks:
        continue # bad suggestion
    
    # Query for targets and their counts for the specific label
    target_query = f"""
        SELECT
            target,
            COUNT(*) AS count
        FROM
            redirect_candidate
        WHERE
            label = %s
        GROUP BY
            target
        ORDER BY
            count DESC
    """

    # Execute target query
    with connection.cursor() as cursor:
        cursor.execute(target_query, (label,))
        target_results = cursor.fetchall()
    is_redirect = False
    if len(target_results) == 1: # only one possible target
        target, _ = target_results[0] # count is useless here
        is_redirect = True
    elif len(target_results) > 1: # possible dab
        dab_targets = []
        # Process each target
        for target_row in target_results:
            target, count = target_row
            if count < DISAMBIG_CUTOFF:
                continue # skip

            ratio = count / total_count * 100

            # Majority target, suggest as redirect
            if ratio >= REDIRECT_PERCENTAGE:
                is_redirect = True
                break
            else:
                dab_targets.append(target)

    if is_redirect:
        # check it's not of the form Foo → Foo (bar) (which is probably a bad suggestion)
        if re.match(fr"^{label} \(.+\)$", target) is not None:
            continue
        insert_redirect_query = """
            INSERT INTO potential_redirects (title, target)
            VALUES (%s, %s)
        """
        with connection.cursor() as cursor:
            cursor.execute(insert_redirect_query, (label, target))
    else: # i.e. dab
        if dab_targets and len(dab_targets) > 1:
            insert_dab_query = """
            INSERT INTO potential_dabs (title, targets)
            VALUES (%s, %s)
        """
            with connection.cursor() as cursor:
                cursor.execute(insert_dab_query, (label, '|'.join(dab_targets)))
    
    if processed_count % BATCH_SIZE == 0: # batches of BATCH_SIZE
        # Commit the changes to the database
        connection.commit()

# Commit any remaining changes
connection.commit()

connection.close()
