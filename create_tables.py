import toolforge
connection = toolforge.toolsdb('s55593__PAGES')

# auto generated from current table setup
query="""
CREATE TABLE badlinks (title varchar(255) NOT NULL  PRIMARY KEY COLLATE utf8_bin);                                                                                                                       
CREATE TABLE badtitles (title varchar(255) NOT NULL  PRIMARY KEY COLLATE utf8_bin);                                                                                                                      
CREATE TABLE key_value_pairs (key_name text NOT NULL  PRIMARY KEY COLLATE utf8mb4_bin, value text  DEFAULT 'NULL'  COLLATE utf8mb4_unicode_ci);                                                          
CREATE TABLE link_map (label varchar(255)  DEFAULT 'NULL'  COLLATE utf8mb4_bin, target varchar(255)  DEFAULT 'NULL'  COLLATE utf8mb4_bin, page_title varchar(255)  DEFAULT 'NULL'  COLLATE utf8mb4_bin); 
CREATE TABLE potential_dabs (title varchar(255) NOT NULL  PRIMARY KEY COLLATE utf8mb4_bin, targets text  DEFAULT 'NULL'  COLLATE utf8mb4_bin);                                                           
CREATE TABLE potential_redirects (title varchar(255) NOT NULL  PRIMARY KEY COLLATE utf8mb4_bin, target varchar(255) NOT NULL  PRIMARY KEY COLLATE utf8mb4_bin);                                          
"""
with connection.cursor() as cursor:
    cursor.execute(query)

connection.commit()
connection.close()