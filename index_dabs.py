import toolforge
import requests

# code from https://www.mediawiki.org/wiki/API:Continue#Example_3:_Python_code_for_iterating_through_all_results


URL = "https://en.wikipedia.org/w/api.php"
    
S = requests.Session()


def query(request):
    request['action'] = 'query'
    request['format'] = 'json'
    lastContinue = {}
    while True:
        # Clone original request
        req = request.copy()
        # Modify it with the values returned in the 'continue' section of the last result.
        req.update(lastContinue)
        # Call API
        result = S.get(URL, params=req).json()
        if 'error' in result:
            raise Exception(result['error'])
        if 'warnings' in result:
            print(result['warnings'])
        if 'query' in result:
            for page in map(lambda item : item['title'], result['query']['categorymembers']):
                yield page
        if 'continue' not in result:
            break
        lastContinue = result['continue']

data = {
    'list': 'categorymembers',
    'cmtitle': 'Category:All disambiguation pages',
    'cmlimit': '500'
}

# takes about 60s to return 300,000 pages (tested on my account)
# cast generator to list
disambiguation_pages = list(query(data))

connection = toolforge.toolsdb('s55593__PAGES')

batch_size = 500
for i in range(0, len(disambiguation_pages), batch_size):
    batch = disambiguation_pages[i:i+batch_size]

    query_bulk_update = "UPDATE key_value_pairs SET value = ']' WHERE key_name IN ({})"
    placeholders_bulk_update = ', '.join(['%s' for _ in batch])
    query_bulk_update = query_bulk_update.format(placeholders_bulk_update)
   
    with connection.cursor() as cursor:
        cursor.execute(query_bulk_update, tuple(batch))
    connection.commit()

connection.close()