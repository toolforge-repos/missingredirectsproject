import toolforge
database_name = 's55593__PAGES'
connection = toolforge.toolsdb(database_name)
# latter two are live queried tables and need backups whilst being updated
tables_to_wipe = ['key_value_pairs', 'link_map'] #, 'potential_dabs', 'potential_redirects']
for table in tables_to_wipe:
   with connection.cursor() as cursor:
      cursor.execute(f'DELETE FROM {table};')
connection.commit()
connection.close()