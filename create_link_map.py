import bz2
from lxml import etree
import toolforge
import mwparserfromhell
from itertools import chain
from lib import printm, get_latest_dump

database_name = 's55593__PAGES'
connection = toolforge.toolsdb(database_name)
file_path = get_latest_dump() 

forbidden_characters = ['#','<','>','[',']', '{', '}', '|']


save_query = "INSERT INTO link_map (label, target, page_title) VALUES (%s, %s, %s)"

def db_save_add_redirect_candidates(redirect_candidates, title):
    global connection
    query = "INSERT INTO link_map (label, target, page_title) VALUES (%s, %s, %s)"
    data = [(label, target, title) for label, target in redirect_candidates]
    try:
        with connection.cursor() as cursor:
            cursor.executemany(query, data)
            connection.commit()
    except: # retry
        try:
            connection = toolforge.toolsdb(database_name)
            with connection.cursor() as cursor:
                cursor.executemany(query, data)
                connection.commit()
        except Exception as e:
            printm(f'Error adding {redirect_candidates} from {title}: {e}')
            return # give up


def fetch_values(wikilinks):
    global connection
    lookups_list = list(chain.from_iterable(wikilinks))  # flatten 2d list
    placeholders = ', '.join(['%s'] * len(lookups_list))
    fetch_key_query = f"SELECT * FROM key_value_pairs WHERE key_name IN ({placeholders});"
    
    # printm(fetch_key_query)
    # printm(lookups_list)
    try:
        with connection.cursor() as cursor:
            cursor.execute(fetch_key_query, lookups_list)
            result = cursor.fetchall()
            return result
    except: # retry
        try:
            connection = toolforge.toolsdb(database_name)
            with connection.cursor() as cursor:
                cursor.execute(fetch_key_query, lookups_list)
                result = cursor.fetchall()
                return result
        except Exception as e:
            printm(f'Error fetching page data for {wikilinks}: {e}')
            return None

def test_and_maybe_store_possible_redirect_candidates(wikilinks, title):
    values = fetch_values(wikilinks)
    if values is None: # error retrieving data
        return
    # values is now a dictionary of titles and their corresponding values (empty string, redirect target or dab)
    values_dict = {row[0]:row[1] for row in values}
    redirect_candidates = []
    for label, target in wikilinks:
        if label in values_dict: # label already exist, so we can't create a redirect from it
            continue
        if target not in values_dict or values_dict[target] == ']': # target doesn't exist or is dab
            continue
        if values_dict[target]:
            target = values_dict[target] # resolve redirect

        if not label or not target: # not target should be implicitly checked for by the second check above, just making it explicit here
           continue
        # printm(f"saving possible redirect '{label}' --> '{target}'")
        redirect_candidates.append((label, target))
    if redirect_candidates:
        db_save_add_redirect_candidates(list(set(redirect_candidates)), title)

capitalise = lambda string : string[0].upper() + string[1:] if string else ''
namespaces = ['Talk', 'User', 'User talk' 'Wikipedia' 'Wikipedia talk', 'File', 'File talk', 'MediaWiki', 'MediaWiki talk', 'Template', 'Template talk', 'Help', 'Help talk', 'Category', 'Category talk', 'Portal', 'Portal talk', 'Draft', 'Draft talk', 'TimedText', 'TimedText talk', 'Module', 'Module talk']

is_cross_namespace = lambda title  : any(title.startswith(f'{ns}:') for ns in namespaces)
 
def process_element(title, text):
    # Parse the string with mwparserfromhell
    wikicode = mwparserfromhell.parse(text)
    
    # Extract all wikilinks
    links = wikicode.filter_wikilinks()
    
    wikilinks = [] # list of tuples
    for link in links:
        # link is a Wikicode object
        # Getting the link destination (title)
        target = capitalise(str(link.title).strip())
        # Handling the displayed text if it's different from the link destination
        if link.text and not link.text.matches(target):

            label = capitalise(str(link.text.strip_code()).strip()) # also remove formatting
            if (label != '') and (target != '')  and not is_cross_namespace(target) and not is_cross_namespace(label) and (len(label) <= 255) and (not any(forbidden_character in label for forbidden_character in forbidden_characters)):
                wikilinks.append((label, target))
    if wikilinks:
        test_and_maybe_store_possible_redirect_candidates(wikilinks, title)

printm('Started processing titles.')
processed_count = 0
with bz2.open(file_path, 'rb') as file:
    context = etree.iterparse(file, events=('end',), tag='{http://www.mediawiki.org/xml/export-0.10/}page')

    for _, elem in context:
        processed_count += 1
        if processed_count % 1000000 == 0:
            printm(f"Processed {processed_count} pages.")
        redirect_element = elem.find('{http://www.mediawiki.org/xml/export-0.10/}redirect')
        if redirect_element is None:
            title_element = elem.find('{http://www.mediawiki.org/xml/export-0.10/}title')
            text_element = elem.find('{http://www.mediawiki.org/xml/export-0.10/}revision/{http://www.mediawiki.org/xml/export-0.10/}text')

            # Ensure the title and text elements are found
            if title_element is not None and text_element is not None:
                title = title_element.text
                text = text_element.text
                process_element(title, text)
            else:
                continue

        elem.clear()  # Clear the element from memory
        while elem.getprevious() is not None:
            del elem.getparent()[0]  # Clear ancestors too
                