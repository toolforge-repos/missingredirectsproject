def printm(text):
    with open('output.txt', 'a') as file:
        file.write(str(text) + '\n')

def get_latest_dump():
    with open('latest_dump.txt', 'r') as file:
        return file.read()