import bz2
from lxml import etree
import toolforge
from lib import printm, get_latest_dump
database_name = 's55593__PAGES'


connection = toolforge.toolsdb(database_name)

def insert_key_value(connection, key, value):
    """
    Insert a key-value pair into the table.
    """
    with connection.cursor() as cursor:
        cursor.execute('INSERT INTO key_value_pairs (key_name, value) VALUES (%s, %s) ON DUPLICATE KEY UPDATE value = %s', (key, value, value))

def process_element(elem, connection):
    global tag_count
    title_element = elem.find('{http://www.mediawiki.org/xml/export-0.10/}title')
    text_element = elem.find('{http://www.mediawiki.org/xml/export-0.10/}revision/{http://www.mediawiki.org/xml/export-0.10/}text')

    # Ensure the title and text elements are found
    if title_element is not None and text_element is not None:
        title = title_element.text
        text = text_element.text
    else:
        return

    # Check pagetype:
    redirect_element = elem.find('{http://www.mediawiki.org/xml/export-0.10/}redirect')
    if redirect_element is not None:
        redirect_target = redirect_element.get('title')
        insert_key_value(connection, title, redirect_target)
    else:
        insert_key_value(connection, title, "")


    elem.clear()  # Clear the element from memory
    while elem.getprevious() is not None:
        del elem.getparent()[0]  # Clear ancestors too

    if tag_count % 10:
        connection.commit()
    if tag_count % 1000000 == 0:
        printm("Processed " + str(tag_count) + " page tags.")
file_path = get_latest_dump()
tag_count = 0


with bz2.open(file_path, 'rb') as file:
    context = etree.iterparse(file, events=('end',), tag='{http://www.mediawiki.org/xml/export-0.10/}page')

    for _, elem in context:
        tag_count += 1
        process_element(elem, connection)

printm("Total " + str(tag_count) + " page tags.")
connection.close()