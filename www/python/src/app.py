# -*- coding: utf-8 -*-
#
# This file is part of the Toolforge Flask + OAuth WSGI tutorial
#
# Copyright (C) 2017 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import flask
import mwoauth
import os
import yaml
import random
import toolforge
import requests
from requests_oauthlib import OAuth1



app = flask.Flask(__name__)

capitalise = lambda string : string[0].upper() + string[1:] if string else ''

# Load configuration from YAML file
__dir__ = os.path.dirname(__file__)
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, 'config.yaml'))))
tool_name = 's55593__PAGES'


def remove_redirect_suggestion(title):
    remove_redirects_suggestion_query = 'DELETE FROM potential_redirects WHERE title = %s'
    connection = toolforge.toolsdb(tool_name)
    with connection.cursor() as cursor:
        cursor.execute(remove_redirects_suggestion_query, (title,))
    connection.commit()

def blacklist_title(title):
    blacklist_title_query = "INSERT INTO badtitles (title) VALUES (%s)"
    connection = toolforge.toolsdb(tool_name)
    with connection.cursor() as cursor:
        cursor.execute(blacklist_title_query, (title,))
    connection.commit()

def badlink_title(title):
    badlink_title_query = "INSERT INTO badlinks (title) VALUES (%s)"
    connection = toolforge.toolsdb(tool_name)
    with connection.cursor() as cursor:
        cursor.execute(badlink_title_query, (title,))
    connection.commit()

def check_authentication():
    if 'username' not in flask.session:
        return jsonify({'status': 'error', 'message': 'User not logged in'})


@app.route('/')
def index():
    username = flask.session.get('username', None)
    return flask.render_template(
        'index.html', username=username, authenticate=flask.session.get('authenticate', None), show_login=True)


@app.route('/login')
def login():
    """Initiate an OAuth login.
    
    Call the MediaWiki server to get request secrets and then redirect the
    user to the MediaWiki server to sign the request.
    """
    consumer_token = mwoauth.ConsumerToken(
        app.config['CONSUMER_KEY'], app.config['CONSUMER_SECRET'])
    try:
        redirect, request_token = mwoauth.initiate(
            app.config['OAUTH_MWURI'], consumer_token)
    except Exception:
        app.logger.exception('mwoauth.initiate failed')
        return flask.redirect(flask.url_for('index'))
    else:
        flask.session['request_token'] = dict(zip(
            request_token._fields, request_token))
        return flask.redirect(redirect)


@app.route('/oauth-callback')
def oauth_callback():
    """OAuth handshake callback."""
    if 'request_token' not in flask.session:
        flask.flash(u'OAuth callback failed. Are cookies disabled?')
        return flask.redirect(flask.url_for('index'))

    consumer_token = mwoauth.ConsumerToken(
        app.config['CONSUMER_KEY'], app.config['CONSUMER_SECRET'])

    try:
        access_token = mwoauth.complete(
            app.config['OAUTH_MWURI'],
            consumer_token,
            mwoauth.RequestToken(**flask.session['request_token']),
            flask.request.query_string)

        identity = mwoauth.identify(
            app.config['OAUTH_MWURI'], consumer_token, access_token)    
    except Exception:
        app.logger.exception('OAuth authentication failed')
    
    else:
        flask.session['access_token'] = dict(zip(
            access_token._fields, access_token))
        flask.session['username'] = identity['username']
        flask.session['authenticate'] = (access_token.key, access_token.secret)

    return flask.redirect(flask.url_for('index'))


@app.route('/logout')
def logout():
    """Log the user out by clearing their session."""
    flask.session.clear()
    return flask.redirect(flask.url_for('index'))

from flask import render_template, request, redirect, url_for, jsonify

@app.route('/create_redirects', methods=['GET', 'POST'])
def create_redirects():
    fetch_redirects_query =  """SELECT title, target
                                FROM potential_redirects
                                ORDER BY RAND()
                                LIMIT %s;"""
    # Make sure user is logged in
    if 'authenticate' not in flask.session:
        return flask.redirect(flask.url_for('index'))

    # results_per_request = 10000

    # Fetch redirects for the current page
    connection = toolforge.toolsdb(tool_name)
    with connection.cursor() as cursor:
        cursor.execute(fetch_redirects_query, (1000,))
        results = cursor.fetchall()

    # Check if there are redirects to display
    if results:
        results = list(results)
        random.shuffle(results)
        redirects = [{'label': row[0], 'target': row[1]} for row in results]
        return render_template(
            'create_redirects.html',
            redirects=redirects,
            authenticate=flask.session['authenticate']
        )
    else:
        # If no redirects, display a message or redirect to another page
        return "No redirects available."

@app.route('/dismiss_suggestion', methods=['POST'])
def dismiss_suggestion():
    if request.method == 'POST':
        if 'authenticate' not in flask.session:
            return jsonify({'status': 'error', 'message': 'User not logged in.'})
        # Get the data from the request's JSON body
        data = request.get_json()

        # Extract relevant information
        label = data.get('label')

        request_type = data.get('type')

        blacklist_title(capitalise(label.strip()))

        if request_type == 'redirect':
            remove_redirect_suggestion(capitalise(label.strip()))
            return jsonify({'status': 'success'})
        
@app.route('/remove_suggestion', methods=['POST'])
def remove_suggestion():
    if request.method == 'POST':
        if 'authenticate' not in flask.session:
            return jsonify({'status': 'error', 'message': 'User not logged in.'})
        # Get the data from the request's JSON body
        data = request.get_json()

        label = data.get('label')

        request_type = data.get('type')

        if request_type == 'redirect':
            remove_redirect_suggestion(capitalise(label.strip()))
            badlink_title(capitalise(label.strip()))
            return jsonify({'status': 'success'})

@app.route('/create_page', methods=['POST'])
def create_page():
    try:
        # Step 2: Fetch user authentication from Flask session
        authenticate = flask.session.get('authenticate', None)

        # Modify the following lines based on your OAuth implementation
        username = flask.session.get('username', None)
        if not authenticate or not username:
            return jsonify({'status': 'error', 'message': 'User not logged in'})

        auth1 = OAuth1(app.config['CONSUMER_KEY'],
               client_secret=app.config['CONSUMER_SECRET'],
               resource_owner_key=authenticate[0],
               resource_owner_secret=authenticate[1])
        
        # Step 3: GET request to fetch CSRF token
        csrf_token = fetch_csrf_token(auth1)

        # Step 4: POST request to edit/create a page
        page_title = request.form.get('page_title')
        page_text = request.form.get('page_text')
        edit_summary = request.form.get('edit_summary')


        result = perform_edit_request(auth1, csrf_token, page_title, page_text, edit_summary)

        return jsonify(result)

    except Exception as e:
        # Handle exceptions appropriately
        return jsonify({'status': 'error', 'message': str(e)})
    

def fetch_csrf_token(auth1):
    URL = "https://en.wikipedia.org/w/api.php"
    PARAMS = {
        "action": "query",
        "meta": "tokens",
        "format": "json"
    }
    response = requests.get(URL, params=PARAMS, auth=auth1)
    data = response.json()
    csrf_token = data['query']['tokens']['csrftoken']
    return csrf_token

def perform_edit_request(auth1, csrf_token, page_title, page_text, edit_summary):
    URL = "https://en.wikipedia.org/w/api.php"
    PARAMS = {
        "action": "edit",
        "title": page_title,
        "token": csrf_token,
        "format": "json",
        "text": page_text,
        "summary": edit_summary,
        "createonly": 1,
        "notminor": 1
    }
    response = requests.post(URL, data=PARAMS, auth=auth1)
    data = response.json()
    if data['edit']['result'] == 'Success':
        remove_redirect_suggestion(capitalise(page_title.strip()))
    return data