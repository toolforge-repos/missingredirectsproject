import os
from lib import printm

def get_latest_folder(path):
    folders = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
    if not folders:
        return None
    folders.sort(reverse=True)
    return folders[0]

base_path = "/mnt/nfs/dumps-clouddumps1002.wikimedia.org/enwiki/"
latest_folder = get_latest_folder(base_path)
if latest_folder is None:
    printm("No folders found.")
else:
    file_loaction = f"{base_path}/{latest_folder}/enwiki-{latest_folder}-pages-articles.xml.bz2"
    with open('latest_dump.text', 'w') as file:
        file.write(file_loaction)